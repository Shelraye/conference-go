from django.http import JsonResponse
from common.json import ModelEncoder
from .models import Conference, Location, State
from django.views.decorators.http import require_http_methods
import json
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
from events.acls import get_weather_data, get_photo


class ConferenceListEncoder(ModelEncoder):
    model = Conference
    properties = [
        "name",
    ]


class LocationListEncoder(ModelEncoder):
    model = Location
    properties = ["name"]


class ConferenceDetailEncoder(ModelEncoder):
    model = Conference
    properties = [
        "name",
        "description",
        "max_presentations",
        "max_attendees",
        "starts",
        "ends",
        "created",
        "updated",
        "location",
    ]
    encoders = {
        "location": LocationListEncoder(),
    }

    # def get_extra_data(self, o):
    #     return {
    #         "weather": get_weather_data(o.location.city, o.location.state.name)
    #     }


class LocationDetailEncoder(ModelEncoder):
    model = Location
    properties = [
        "name",
        "city",
        "room_count",
        "created",
        "updated",
        "state",
        "picture_url",
    ]

    def get_extra_data(self, o):
        return {
            "state": o.state.abbreviation,
            "photo_url": get_photo(o.city, o.state.name),
        }


@require_http_methods(["GET", "POST"])
def api_list_conferences(request):
    conferences = Conference.objects.all()
    if request.method == "POST":
        params = json.loads(request.body)
        try:
            location = Location.objects.get(id=params["location"])
            params["location"] = location
        except Location.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location id"},
                status=400,
            )
        conference = Conference.objects.create(**params)
        return JsonResponse(
            {"conferences": conferences}, encoder=ConferenceListEncoder
        )
    else:
        conference = Conference.objects.all()
        return JsonResponse(
            conference,
            encoder=ConferenceDetailEncoder,
            safe=False,
        )


# try except for the weather
# dumps () to change none to null


@require_http_methods(["GET", "DELETE", "PUT"])
def api_show_conference(request, pk):
    if request.method == "DELETE":
        count, _ = Conference.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
    elif request.method == "PUT":
        try:
            params = json.loads(request.body)
            location = Location.objects.get(id=params["location"])
            params["location"] = location
        except Location.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location id"},
                status=400,
            )
        conference = Conference.objects.filter(id=pk).update(**params)
        return JsonResponse(
            conference,
            encoder=ConferenceDetailEncoder,
            safe=False,
        )
    else:
        conference = Conference.objects.get(id=pk)
        weather = get_weather_data(
            conference.location.city, conference.location.state.name
        )
        dictionary = {"conference": conference, "weather": weather}
        return JsonResponse(
            dictionary, encoder=ConferenceDetailEncoder, safe=False
        )


@require_http_methods(["GET", "POST"])
def api_list_locations(request):
    # location = Location.objects.all()
    if request.method == "POST":
        params = json.loads(request.body)
        try:
            state = State.objects.get(abbreviation=params["state"])
            params["state"] = state
        except State.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid state abbreviation"},
                status=400,
            )
        location = Location.objects.create(**params)
        return JsonResponse(
            location, encoder=LocationDetailEncoder, safe=False
        )
    else:
        location = Location.objects.all()
        return JsonResponse(
            {"location": location},
            encoder=LocationListEncoder,
        )


@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_location(request, pk):
    if request.method == "DELETE":
        count, _ = Location.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
    elif request.method == "PUT":
        try:
            params = json.loads(request.body)
            state = State.objects.get(abbreviation=params["state"])
            params["state"] = state
        except State.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid state abbreviation"},
                status=400,
            )
        location = Location.objects.get(id=pk)
        return JsonResponse(
            location,
            encoder=LocationDetailEncoder,
            safe=False,
        )

    else:
        location = Location.objects.get(id=pk)
        return JsonResponse(
            location,
            encoder=LocationDetailEncoder,
            safe=False,
        )
