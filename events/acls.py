from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import json
import requests


def get_photo(city, state):
    # Use the Pexels API
    headers = {"Authorization": PEXELS_API_KEY}
    params = {
        "per_page": 1,
        "query": city + " " + state,
    }
    url = "https://api.pexels.com/v1/search"
    response = requests.get(url, params=params, headers=headers)
    content = json.loads(response.content)
    try:
        return {"pictures_url": content["photos"][0]["src"]["original"]}
    except:
        return {"picture_url": None}

    # per page is an optional paramter
    # response = requests.get("https://api.pexels.com/v1/search?valid=true")
    # pexels = json.loads(response.content)

    # data_pexels = {"picture_url": pexels[" "], "api_key": "PEXELS_API_KEY" }
    # res = requests.get(url, params=params)


# params = {'start_date': '2022-09-14', 'end_date': '2022-09-14', 'api_key': 'DEMO_KEY'}

# res = requests.get(url, params=params)


def get_weather_data(city, state):
    # Use the Open Weather API
    params = {"q": city + "," + state + "US", "appid": OPEN_WEATHER_API_KEY}
    url = "http://api.openweathermap.org/geo/1.0/direct"
    response = requests.get(url, params=params)
    content = json.loads(response.content)
    latitude = content[0]["lat"]
    longitude = content[0]["lon"]
    weather_url = "https://api.openweathermap.org/data/2.5/weather"
    params = {
        "lat": latitude,
        "lon": longitude,
        "units": "imperial",
        "appid": OPEN_WEATHER_API_KEY,
    }
    response = requests.get(weather_url, params=params)
    content = json.loads(response.content)
    return {
        "temp": content["main"]["temp"],
        "description": content["weather"][0]["description"],
    }

    # return {"weather": content["temp"]["description"]}

    # use a print statement to make sure the right stuff is coming through.

    # try:
    #     return {"main_temp": content["decsription"][0]}
    # except:
    #     return {"main_temp": None}

    # url = "https://api.openweathermap.org/data/2.5/weather"
    # params = {
    #     "lat": content, "lon": content,
    # }
    # response = requests.get(url, params=params)
    # weather = json.loads(response.content)
    # try:
    #     return {"main_temp": weather["decsription"][0]}
    # except:
    #     return {"main_temp": None}


# ?q={city name},{state code},{country code}&limit={limit}&appid={API key}
