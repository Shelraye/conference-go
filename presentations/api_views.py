from django.http import JsonResponse
from common.json import ModelEncoder
from events.api_views import ConferenceListEncoder
from .models import Presentation
from events.models import Conference
import json
from django.views.decorators.http import require_http_methods


class PresentationListEncoder(ModelEncoder):
    model = Presentation
    properties = [
        "title",
        "status",
    ]

    def get_extra_data(self, o):
        return {"status": o.status.name}


class PresentationDetailEncoder(ModelEncoder):
    model = Presentation
    properties = [
        "presenter_name",
        "company_name",
        "presenter_email",
        "title",
        "synopsis",
        "created",
        "status",
        "conference",
    ]

    def get_extra_data(self, o):
        return {"status": o.status.name}

    encoders = {
        "conference": ConferenceListEncoder(),
    }


# params["conference"] = Presentation.objects.filter(
#     conference=conference_id
# )
# presentations = Presentation.objects.filter(
#     conference=conference_id
# ).create(**params)
# presentations = Presentation.objects.filter(conference=conference_id)


@require_http_methods(["GET", "POST"])
def api_list_presentations(request, conference_id):
    if request.method == "POST":
        params = json.loads(request.body)
        try:
            conference = Conference.objects.get(id=conference_id)
            params["conference"] = conference
        except Conference.DoesNotExist:
            return JsonResponse({"message": "conference ID not chosen"})
        presentations = Presentation.create(**params)
    else:
        presentations = Presentation.objects.all()
        return JsonResponse(
            {"presentations": presentations},
            encoder=PresentationListEncoder,
            safe=False,
        )

    # def api_show_presentation(request, pk):


def api_show_presentation(request, pk):
    presentation = Presentation.objects.get(id=pk)
    return JsonResponse(
        presentation, encoder=PresentationDetailEncoder, safe=False
    )
