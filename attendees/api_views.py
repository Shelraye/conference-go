from django.http import JsonResponse
from common.json import ModelEncoder
from .models import Attendee
from events.models import Conference
from events.api_views import ConferenceListEncoder
from django.views.decorators.http import require_http_methods
import json


class AttendeeListEncoder(ModelEncoder):
    model = Attendee
    properties = [
        "name",
    ]
    # encoders = {
    #     "attendees": AttendeeListEncoder(),
    # } - ask if we're supposed to put an encoders here for the additional
    #  attributes
    # - ie on line 31 in api_views for events


class AttendeeDetailEncoder(ModelEncoder):
    model = Attendee
    properties = [
        "email",
        "name",
        "company_name",
        "created",
        "conference",
    ]
    encoders = {
        "conference": ConferenceListEncoder(),
    }


@require_http_methods(["GET", "POST"])
def api_list_attendees(request, conference_id):
    attendees = Attendee.objects.get(id=conference_id)
    if request.method == "POST":
        params = json.loads(request.body)
        try:
            conference = Conference.objects.get(id=conference_id)
            params["conference"] = conference
        except Conference.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid conference id"},
                status=400,
            )
        attendees = Attendee.objects.create(**params)
        return JsonResponse(
            {"attendee": attendees}, encoder=AttendeeListEncoder
        )
    else:
        attendees = Attendee.objects.get(id=conference_id)
        return JsonResponse(
            {"attendees": attendees}, encoder=AttendeeListEncoder
        )


def api_show_attendee(request, pk):
    attendee = Attendee.objects.get(id=pk)
    return JsonResponse(attendee, encoder=AttendeeDetailEncoder, safe=False)
